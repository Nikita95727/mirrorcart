<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class category extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'category_name' => 'Ноутбуки',
            'category_code' => 'laptops'
        ]);

        DB::table('categories')->insert([
            'category_name' => 'Смартфоны',
            'category_code' => 'smartphones'
        ]);
    }
}
