<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $category = App\category::find(1);
        $vendor = App\vendor::find(2);

        for ($i=0; $i<100; $i++) {
            App\product::create(array(
                'title' => $faker->sentence,
                'description' => $faker->text,
                'price' => $faker->randomNumber(4),
                'image' => $faker->imageUrl('480', '160'),
                'categories_id' => $category->id,
                'vendor_id' => $vendor->id
            ));
        }
    }
}
