<?php

use Illuminate\Database\Seeder;
use App\role;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = new role();
        $adminRole->roleName = 'admin';
        $adminRole->save();
        $contentManagerRole = new role();
        $contentManagerRole->roleName = 'content manager';
        $contentManagerRole->save();
        $userRole = new role();
        $userRole->roleName = 'user';
        $userRole->save();
    }
}
