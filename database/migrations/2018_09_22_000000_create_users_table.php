<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->char('name',100);
            $table->char('surname',100);
            $table->char('email',100)->unique();
            $table->char('password', 255);
            $table->timestamp('email_verified_at')->nullable();
            $table->date('customer_birthdate');
            $table->char('avatar')->nullable();
            $table->unsignedInteger('role_id')->default(3);
            $table->timestamps();

            $table->foreign('role_id')
                ->references('id')
                ->on('roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
