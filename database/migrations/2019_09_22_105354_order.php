<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Order extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('product')->unsigned()->nullable();
            $table->foreign('product')->references('id')->on('products');
            $table->integer('shipping')->unsigned()->nullable();
            $table->integer('customer')->unsigned()->nullable();
            $table->foreign('customer')->references('id')->on('users');
            $table->foreign('shipping')->references('id')->on('shippments');
            $table->double('final_price');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
