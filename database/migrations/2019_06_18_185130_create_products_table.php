<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->char('title',255);
            $table->longText('description');
            $table->double('price');
            $table->char('image');
            $table->unsignedInteger('categories_id')->default(1);
            $table->unsignedInteger('vendor_id')->default(1);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('categories_id')->references('id')->on('categories');
            $table->foreign('vendor_id')->references('id')->on('vendors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
