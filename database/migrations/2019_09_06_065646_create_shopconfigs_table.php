<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopconfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopconfigs', function (Blueprint $table) {
            $table->increments('id');
            $table->char('shopName');
            $table->char('shopPhoneNumber', 20);
            $table->char('shopLogo', 255);
            $table->char('shopAddress');
            $table->char('shopEmail');
            $table->char('shopCurrency')->default('USD');
            $table->char('shopLocale')->default('EN');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopconfigs');
    }
}
