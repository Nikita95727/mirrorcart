<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $categories = App\category::all();
    return view('welcome', compact(array(
        'shopConfig', 'categories'
    )));
});

Route::get('/admin', 'AdminController@index');
Route::get('/shop', 'ShopController@index');
Route::get('/shop/buy/{product}', 'ShopController@buy');
Route::get('/shop/addtowishlist/{product}', 'ShopController@addtowishlist');
Route::get('/shop/cart', 'ShopController@cart');
Route::get('/shop/cart/remove/{product}', 'ShopController@remove');
Route::get('shop/show/{product}', 'ProductController@show');
Route::get('/shop/product/edit/{product}', 'AdminController@editProduct');
Route::get('/shop/product/delete/{product}', 'ProductController@delete');
Route::post('/shop/product/addcomment', 'ProductController@addcomment');

// Маршруты для панели пользователя
Route::get('/dashboard', 'DashboardController@index')->name('dashboard')->middleware('verified');
Route::get('/dashboard/info', 'DashboardController@info');
Route::get('/dashboard/address', 'DashboardController@address');
Route::get('dashboard/address/createAddress', 'DashboardController@create');
Route::get('/dashboard/equal', 'DashboardController@equal');
Route::get('/dashboard/wishes', 'DashboardController@wishes');
Route::get('/shop/admin/showItems', 'AdminController@showItems');
Route::get('/shop/admin/createItem', 'AdminController@create');
Route::get('/shop/contacts', 'ShopController@contacts');
Route::post('/dashboard/changeAvatar', 'UserController@changeAvatar');
Route::post('/dashboard/changeName', 'UserController@changeName');
Route::post('/dashboard/changeEmail', 'UserController@changeEmail');
Route::post('/dashboard/addShippingAddress', 'ShippingAddressController@store');
Route::get('/dashboard/removeShippingAddress/{shippingAddress}', 'ShippingAddressController@remove');

// Маршруты для экспорта-импорта продуктов
Route::get('/shop/admin/export', 'ProductController@export');
Route::get('/admin/importproducts', 'AdminController@importProducts');

// Маршруты для админки
Route::get('/shop/admin/managecustomers', 'AdminController@manageUsers');
Route::get('/shop/admin/editUser/{user}', 'AdminController@editUser');
Route::get('/shop/admin/createUser', 'AdminController@createUser');
Route::get('/shop/admin/showUsers', 'AdminController@showUsers');
Route::get('/shop/admin/users/import', 'AdminController@importUsers');
Route::get('/shop/admin/sales', 'AdminController@sales');

// Маршруты для взаимодействия с пользователями
Route::get('/shop/admin/users/export', 'UserController@export');
Route::get('/shop/admin/users/remove/{user}', 'UserController@remove');
Route::get('/shop/admin/users/showUser/{user}', 'UserController@show');
Route::post('/shop/admin/user/store', 'UserController@store');

// Маршруты для взаимодействия с ролями пользователя
Route::get('/shop/admin/user/manageRoles', 'AdminController@manageRoles');
Route::get('/shop/admin/user/remove/{role}', 'RoleController@remove');
Route::post('/shop/admin/user/storeRole', 'RoleController@store');
Route::post('/shop/admin/user/changeRole', 'UserController@changeRole');

// Маршруты для подарочной карты
Route::get('/shop/admin/infogiftcard', 'AdminController@infoGiftCard');
Route::get('/shop/admin/addgiftcard', 'AdminController@addGiftCard');
Route::get('/shop/deletegiftcard/{giftCard}','GiftCardController@deleteGiftCard');
Route::get('/shop/removegiftcard','GiftCardController@removeGiftCard');
Route::post('/shop/admin/savegiftcard', 'GiftCardController@storeGiftCard');
Route::post('/shop/verifygift', 'GiftCardController@verifyGiftCard');

// Маршруты для методов доставки
Route::get('/shop/admin/infoshippingmethods', 'AdminController@manageShippingMethods');
Route::get('/shop/admin/addshippingmethods', 'AdminController@createShippingMethods');
Route::get('/shop/admin/editshippingmethods/{shipping}', 'AdminController@updateShippingMethods');
Route::get('/shop/deleteshipping/{shipping}', 'ShippingController@deleteShipping');
Route::post('/shop/admin/saveshipping', 'ShippingController@store');

// Маршруты для купонов
Route::get('/shop/admin/addcupon', 'AdminController@addCupon');
Route::get('/shop/admin/infocupon', 'AdminController@infoCupon');
Route::get('/shop/deletecupon/{cupon}','CuponController@deleteCupon');   //передали модель
Route::post('/shop/admin/savecupon', 'CuponController@storeCupon');
Route::post('/shop/verifycupon', 'CuponController@verifyCupon');

// Маршруты для управления категориями
Route::get('/shop/admin/managecategories', 'AdminController@manageCategories');
Route::get('/shop/admin/createcategory', 'AdminController@createCategory');
Route::get('/shop/admin/deletecategory/{category}', 'CategoryController@deleteCategory');
Route::post('/shop/storeCategory', 'CategoryController@store');

// Маршруты для поиска и фильтрации по каталогу
Route::get('/shop/category/{category}', 'CategorySearch@categorySearch');
Route::get('/shop/sort/asc', 'CategorySearch@sortAsc');
Route::get('/shop/sort/desc', 'CategorySearch@sortDesc');

Route::post('/item/store','ProductController@store');
Route::post('/dashboard/address/create','ShippingAddressController@store');
Route::post('/item/import', 'ProductController@import');
Route::post('/user/import', 'UserController@import');

// Маршруты для корзины
Route::get('/shop/cart/addqty/{product}', 'CartController@addQty');
Route::get('/shop/cart/minusqty/{product}', 'CartController@minusQty');
Route::get('/shop/cart/clearAll', 'CartController@clearAll');

// Маршруты для взаимодействия с листом сравнений
Route::get('/shop/comparelist/add/{product}', 'CompareListController@add');

// Маршруты для взаимодействия с листом пожеланий
Route::get('/shop/addtowishlist/{product}', 'WishlistController@addToWishlist');

// Маршруты для конфигурации магазина
Route::get('/shop/admin/storeconfig', 'AdminController@storeConfig');
Route::post('/shop/storeShopPhoneNumber', 'ShopconfigController@storeShopPhoneNumber');
Route::post('/shop/storeShopName', 'ShopconfigController@storeShopName');
Route::post('/shop/storeShopLogo', 'ShopconfigController@storeShopLogo');
Route::post('/shop/storeShopAddress', 'ShopconfigController@storeShopAddress');
Route::post('/shop/storeShopEmail', 'ShopconfigController@storeShopEmail');
Auth::routes();
Auth::routes(['verify' => true]);

// Маршруты для взаимодействия с производителями
Route::get('/admin/manageVendors', 'AdminController@manageVendors');
Route::get('/admin/deleteVendor/{vendor}', 'VendorController@deleteVendor');
Route::post('/admin/storeVendor', 'VendorController@storeVendor');

// Маршруты для взаимодействия с кэшем магазина
Route::get('/admin/manageCache', 'AdminController@manageCache');
Route::get('/admin/cacheClear', 'AdminController@clearCache');
