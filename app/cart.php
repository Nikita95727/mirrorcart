<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cart extends Model
{
    public function getGiftCard()
    {
        if (session()->exists('giftcardCode')) {
            return session()->get('giftcardCode');
        }
        return 0;
    }

    public function deleteGiftCard()
    {
        if (session()->exists('giftcardCode')) {
            session()->forget('giftcardCode');
        }
    }

    public function getTotal()
    {
        if (session()->exists('cart')) {
            $total = 0;
            foreach (session('cart') as $id => $cartItems) {
                $total += $cartItems['price'] * $cartItems['qty'];
            }
            return $total;
        }
        return 0;
    }

    public function getSubtotal()
    {
        if (session()->exists('giftcardCode')) {
            return $this->getTotal()-session()->get('giftcardCode');
        }

        if (session()->exists('Cupon')) {
            return $this->getTotal()-session()->get('Cupon');
        }

        return $this->getTotal();
    }

    public function getCupon()
    {
        if (session()->exists('Cupon')) {
            return session()->get('Cupon');
        }

        return 0;
    }

    /*
    public function getInterestingProducts()
    {
        $cart = session()->get('cart');
        $category = $cart['category_code'];
        $interestingProducts = product::where('category_code', $category)->get();
        return $interestingProducts;
    }*/
}
