<?php

namespace App\Imports;

use App\product;
use Maatwebsite\Excel\Concerns\ToModel;

class ProductImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new product(array(
            'category_code' => $row[2],
            'image' => $row[3],
            'title' => $row[4],
            'description' => $row['5'],
            'price' => $row['6']
        ));
    }
}
