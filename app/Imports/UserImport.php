<?php

namespace App\Imports;

use App\User;
use Maatwebsite\Excel\Concerns\ToModel;

class UserImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new User([
            'name' => $row[0],
            'surname' => $row[1],
            'email' => $row[2],
            'customer_birthdate' => $row[3],
            'password' => md5($row[4])
        ]);
    }
}
