<?php

namespace App\Http\Controllers;

use App\Exports\UserExport;
use App\Imports\UserImport;
use App\User;
use App\user_role;
use Illuminate\Http\Request;
use App\product;
use Excel;

class UserController extends Controller
{
    public function import(Request $request)
    {
        $this->validate($request, [
            'importFile' => 'required'
        ]);

        Excel::import(new UserImport(), $request->file('importFile'));

        return redirect('/shop/admin/showUsers');
    }

    public function export()
    {
        return Excel::download(new UserExport(), 'users.xlsx');
    }

    public function remove($id)
    {
        User::find($id)->delete();
        return redirect()->back()->with('success', __('Пользователь был успешно удалён'));
    }

    public function show($id)
    {
        $user = User::find($id);

        return view('admin.users.showuser', compact('user'));
    }

    public function changeRole(Request $request)
    {
        $this->validate($request, [
            'userId' => 'required',
            'userRole' => 'required'
        ]);

        if ($request->post('userId')) {
            $id = $request->post('userId');
            $user = User::find($id);
            $user->role_id = $request->input('userRole');
            $user->save();
            return redirect()->back()->with('success', __('Роль была успешно изменена'));
        }

        return redirect()->back()->with('error', __('Произошла ошибка'));
    }

    public function changeAvatar(Request $request)
    {
        $this->validate($request, [
            'userId' => 'required',
            'userAvatar' => 'required'
        ]);

        if ($request->post('userId')) {
            $id = $request->post('userId');
            $user = User::find($id);
            $user->avatar = $request->post('userAvatar');
            $user->save();
            return redirect()->back()->with('success', __('Фото профиля было успешно изменено'));
        }

        return redirect()->back()->with('error', __('Произошла ошибка'));
    }

    public function changeName(Request $request)
    {
        $this->validate($request, [
            'userId' => 'required',
            'userName' => 'required',
            'userSurname' => 'required'
        ]);

        if ($request->post('userId')) {
            $id = $request->post('userId');
            $user = User::find($id);
            $user->name = $request->post('userName');
            $user->surname = $request->post('userSurname');
            $user->save();
            return redirect()->back()->with('success', __('Имя было успешно изменено'));
        }

        return redirect()->back()->with('error', __('Произошла ошибка'));
    }

    public function changeEmail(Request $request)
    {
        $this->validate($request, [
            'userId' => 'required',
            'userEmail' => 'required',
        ]);

        if ($request->post('userId')) {
            $id = $request->post('userId');
            $user = User::find($id);
            $user->email = $request->post('userEmail');
            $user->save();
            return redirect()->back()->with('success', __('Адрес электронной почты был успешно изменён'));
        }

        return redirect()->back()->with('error', __('Произошла ошибка'));
    }

    public function store(Request $request)
    {
        $this->validate($request, array(
           'userId' => 'required',
           'name' => 'required',
           'surname' => 'required',
           'customer_birthdate' => 'required',
           'email' => 'required'
        ));

        $user = User::find($request->post('userId'));
        $user->name = $request->post('name');
        $user->surname = $request->post('surname');
        $user->customer_birthdate = $request->post('customer_birthdate');
        $user->email = $request->post('email');
        $user->save();

        return redirect('/shop/admin/showUsers')->with('success', __('Данные пользователя успешно обновлены'));
    }
}
