<?php

namespace App\Http\Controllers;

use App\product;
use Illuminate\Http\Request;
use App;
use Illuminate\Support\Facades\Auth;
use App\shopconfig;
use Illuminate\Support\Facades\Artisan;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        if (Auth::check()) {
            if ($this->_getUser()->hasAccess()) {
                return view('admin.index');
            }
        }

        return redirect('/');
    }

    public function showItems()
    {
        if (Auth::check()) {
            if ($this->_getUser()->hasAccess()) {
                $products = App\product::paginate(3);
                return view('admin.products.showproducts',compact('products'));
            }
        }

        return redirect('/');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::check()) {
            if ($this->_getUser()->hasAccess()) {
                $vendors = App\vendor::all();
                $categories = App\category::all();
                return view('admin.products.createproduct', compact(
                    array('categories', 'vendors')
                ));
            }
        }

        return redirect('/');
    }

    public function editProduct($id)
    {
        if (Auth::check()) {
            if ($this->_getUser()->hasAccess()) {
                $product = App\product::find($id);
                $vendors = App\vendor::all();
                $categories = App\category::all();
                return view('admin.products.editproduct', compact(
                    array('categories', 'vendors', 'product')
                ));
            }
        }

        return redirect('/');
    }

    public function manageUsers()
    {
        if (Auth::check()) {
            if ($this->_getUser()->hasAccess()) {
                return view('admin.users.manage');
            }
        }

        return redirect('/');
    }

    public function createUser()
    {
        if (Auth::check()) {
            if ($this->_getUser()->hasAccess()) {
                return view('admin.users.createuser');
            }
        }

        return redirect('/');
    }

    public function editUser($id)
    {
        if (Auth::check()) {
            if ($this->_getUser()->hasAccess()) {
                $user = App\User::find($id);
                return view('admin.users.edituser', compact('user'));
            }
        }

        return redirect('/');
    }

    public function showUsers()
    {
        if (Auth::check()) {
            if ($this->_getUser()->hasAccess()) {
                $users = App\User::paginate(3);
                return view('admin.users.showusers', compact('users'));
            }
        }

        return redirect('/');
    }

    public function importUsers()
    {
        if (Auth::check()) {
            if ($this->_getUser()->hasAccess()) {
                return view('admin.users.importusers');
            }
        }

        return redirect('login');
    }

    public function manageRoles()
    {
        if (Auth::check()) {
            if ($this->_getUser()->hasAccess()) {
                $roles = App\role::all();
                return view('admin.users.roles.showroles', compact('roles'));
            }
        }

        return redirect('/');
    }

    public function importProducts()
    {
        if (Auth::check()) {
            if ($this->_getUser()->hasAccess()) {
                return view('admin.products.importproducts');
            }
        }

        return redirect('/');
    }

    public function sales()
    {
        if (Auth::check()) {
            if ($this->_getUser()->hasAccess()) {
                return view('admin.sales');
            }
        }

        return redirect('/');
    }

    public function infoGiftCard()
    {
        if (Auth::check()) {
            if ($this->_getUser()->hasAccess()) {
                $giftcards = App\giftCard::paginate(3);
                return view('admin.giftcard.infogiftcard', compact('giftcards'));
            }
        }

        return redirect('/');
    }

    public function addGiftCard()
    {
        if (Auth::check()) {
            if ($this->_getUser()->hasAccess()) {
                return view('admin.giftcard.addgiftcard');
            }
        }

        return redirect('/');
    }

    public function manageShippingMethods()
    {
        if (Auth::check()) {
            if ($this->_getUser()->hasAccess()) {
                $shippments = App\shipping::paginate(3);
                return view('admin.shippingMethods.infoshippingmethods', compact('shippments'));
            }
        }

        return redirect('/');
    }

    public function createShippingMethods()
    {
        if (Auth::check()) {
            if ($this->_getUser()->hasAccess()) {
                return view('admin.shippingMethods.addshippingmethods');
            }
        }

        return redirect('/');
    }

    public function updateShippingMethods($id)
    {
        if (Auth::check()) {
            if ($this->_getUser()->hasAccess()) {
                $shippment = App\shipping::find($id);
                return view('admin.shippingMethods.editshippingmethods', compact('shippment'));
            }
        }

        return redirect('/');
    }

    public function infoCupon()
    {
        if (Auth::check()) {
            if ($this->_getUser()->hasAccess()) {
                $cupons = App\cupon::paginate(3);
                return view('admin.cupon.infocupon', compact('cupons'));
            }
        }

        return redirect('/');
    }

    public function addCupon()
    {
        if (Auth::check()) {
            if ($this->_getUser()->hasAccess()) {
                return view('admin.cupon.addcupon');
            }
        }

        return redirect('/');
    }

    public function manageCategories()
    {
        if (Auth::check()){
            if ($this->_getUser()->hasAccess()) {
                $categories = App\category::paginate(3);
                return view('admin.categories.infocategories', compact('categories'));
            }
        }

        return redirect('/');
    }

    public function storeConfig()
    {
        if (Auth::check()) {
            if ($this->_getUser()->hasAccess()) {
                $shopconfig = shopconfig::find(1);
                return view('admin.storeconfig.infostoreconfig', compact('shopconfig'));
            }
        }

        return redirect('/');
    }

    public function manageVendors()
    {
        if (Auth::check()) {
            if ($this->_getUser()->hasAccess()) {
                $vendors = App\vendor::all();
                return view('admin.vendor.infovendor', compact('vendors'));
            }
        }

        return redirect('/');
    }

    public function manageCache()
    {
        if (Auth::check()) {
            if ($this->_getUser()->hasAccess()) {
                return view('admin.cache.manage');
            }
        }

        return redirect('/');
    }

    public function clearCache()
    {
        if (Auth::check()) {
            if ($this->_getUser()->hasAccess()) {
                Artisan::call('cache:clear');
                Artisan::call('config:cache');
                Artisan::call('view:clear');
                Artisan::call('route:clear');
                Artisan::call('backup:clean');
                return redirect()->back()->with('success', 'Кэш успешно очищен');
            }
        }

        return redirect('/');
    }

    private function _getUser()
    {
        return Auth::user();
    }
}
