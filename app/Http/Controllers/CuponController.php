<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App;

class CuponController extends Controller
{
    public function storeCupon(Request $request)
    {
    	$this->validate($request, [
    		'cuponcode' => 'required',
            'nominal' => 'required',
    		'selectstatus' => 'required'
    	]);

    	$cupon = new App\cupon;
    	switch ($request->input('selectstatus')) {
    		case 'active':
    			$cupon->active = 1;
    			break;

    		case 'inactive':
    			$cupon->active = 0;
    			break;
    	}
    	$cupon->cuponCode = $request->input('cuponcode');
        $cupon->nominal = $request->input('nominal');
    	$cupon->save();
    	return redirect('/shop/admin/infocupon')->with('success', __('Код купона был успешно добавлен'));
    }

     public function deleteCupon($id)
    {
    	App\cupon::find($id)->delete();
    	return redirect()->back()->with('success', __('Код купона успешно удален'));
    }

    public function verifyCupon(Request $request)
    {
        $this->validate($request, [
            'verifycupon' => 'required',
        ]);

        $verify = App\cupon::where('cuponCode', $request->input('verifycupon'))->get();
        if (isset($verify[0]['cuponCode'])) {
            if ($verify[0]['cuponCode'] === $request->input('verifycupon')) {
                session()->put('Cupon', $verify[0]['nominal']);  //создали перем.Купон для помещения Номинала
                return redirect()->back()->with('success', __('Успешно добавлено'));
            }
        }

        return redirect('/shop/show')->with('error', __('Возникла ошибка при добавлении'));
    }
}
