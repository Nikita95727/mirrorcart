<?php

namespace App\Http\Controllers;

use App\product;
use App\wishlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WishlistController extends Controller
{
    public function addToWishlist($id)
    {
        if (Auth::check()) {
            $product = product::find($id);
            $userId = Auth::user()->id;
            $wishlist = new wishlist();
            $wishlist->product_id = $product->id;
            $wishlist->product_title = $product->title;
            $wishlist->product_image = $product->image;
            $wishlist->user_id = $userId;
            $wishlist->save();
            return redirect()->back()->with('success', __('Продукт успешно добавлен в список желаний'));
        }

        return redirect('login');
    }
}
