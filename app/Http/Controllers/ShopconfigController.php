<?php

namespace App\Http\Controllers;

use App\shopconfig;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ShopconfigController extends Controller
{
    public function storeShopName(Request $request)
    {
        if (Auth::check()) {
            if ($request->input('shopconfigId')) {
                $id = $request->input('shopconfigId');
                $shopconfig = shopconfig::find($id);
                $shopconfig->shopName = $request->input('shopConfigName');
                $shopconfig->save();
                return redirect()->back()->with('success', __('Имя магазина изменено'));
            }
        }

        return redirect()->back()->with('error', 'Доступ запрещён');
    }

    public function storeShopPhoneNumber(Request $request)
    {
        if (Auth::check()) {
            if ($request->input('shopconfigId')) {
                $id = $request->input('shopconfigId');
                $shopconfig = shopconfig::find($id);
                $shopconfig->shopPhoneNumber = $request->input('shopConfigPhoneNumber');
                $shopconfig->save();
                return redirect()->back()->with('success', __('Номер телефона магазина изменён'));
            }
        }

        return redirect()->back()->with('error', __('Доступ запрещён'));
    }

    public function storeShopLogo(Request $request)
    {
        if (Auth::check()) {
            if ($request->input('shopconfigId')) {
                $id = $request->input('shopconfigId');
                $shopconfig = shopconfig::find($id);
                $shopconfig->shopLogo = $request->input('shopConfigLogo');
                $shopconfig->save();
                return redirect()->back()->with('success', __('Логотип магазина успешно изменён'));
            }
        }

        return redirect()->back()->with('error', __('Доступ запрещён'));
    }

    public function storeShopAddress(Request $request)
    {
        if (Auth::check()) {
            if ($request->input('shopconfigId')) {
                $id = $request->input('shopconfigId');
                $shopconfig = shopconfig::find($id);
                $shopconfig->shopAddress = $request->input('shopConfigAddress');
                $shopconfig->save();
                return redirect()->back()->with('success', __('Адрес магазина успешно изменён'));
            }
        }

        return redirect()->back()->with('error', __('Доступ запрещён'));
    }

    public function storeShopEmail(Request $request)
    {
        if (Auth::check()) {
            if ($request->input('shopconfigId')) {
                $id = $request->input('shopconfigId');
                $shopconfig = shopconfig::find($id);
                $shopconfig->shopEmail = $request->input('shopConfigEmail');
                $shopconfig->save();
                return redirect()->back()->with('success', __('Электронный адрес магазина успешно изменён'));
            }
        }

        return redirect()->back()->with('error', __('Доступ запрещён'));
    }
}
