<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class ShippingAddressController extends Controller
{

    public function remove($id)
    {
        App\shippingAddress::find($id)->delete();
        return redirect('/dashboard/address')->with('success', __('Адрес успешно удалён'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'userCity' => 'required',
            'userAddress' => 'required',
            'userPost' => 'required',
            'userTelephone' => 'required',
            'userId' => 'required'
        ]);

        $address = new App\shippingAddress;
        $address->city = $request->post('userCity');
        $address->address = $request->post('userAddress');
        $address->post_index = $request->post('userPost');
        $address->telephone = $request->post('userTelephone');
        $address->user_id = $request->post('userId');
        $address->save();
        return redirect('/dashboard/address')->with('success', __('Адрес успешно добавлен'));
    }
}
