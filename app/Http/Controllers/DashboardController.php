<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
        if (Auth::check()) {
            return view('dashboard.index');
        }
        return redirect('login');

    }

    public function info()
    {
        if (Auth::check()) {
            return view('dashboard.info');
        }

        return redirect('login');
    }

    public function address()
    {
        if (Auth::check()) {
            $shippingAddresses = App\shippingAddress::all();
            return view('dashboard.address', compact('shippingAddresses'));
        }
        return redirect('login');
    }

    public function create()
    {
        if (Auth::check()) {
            return view('dashboard.createAddress');
        }

        return redirect('login');
    }

    public function equal()
    {
        if (Auth::check()) {
            $compareListItems = Auth::user()->getCompareList();
             return view('dashboard.equal', compact('compareListItems'));
        }

        return redirect('login');
    }

    public function wishes()
    {
        if (Auth::check()) {
            $wishlistItems = App\wishlist::where('user_id', Auth::user()->id)->get();
            return view('dashboard.wishes', compact('wishlistItems'));
        }

        return redirect('login');
    }
}
