<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CompareList;
use Illuminate\Support\Facades\Auth;

class CompareListController extends Controller
{
    public function add($productId)
    {
        if (Auth::check()) {
            $compareListItem = new CompareList();
            $compareListItem->user_id = Auth::user()->id;
            $compareListItem->product_id = $productId;
            $compareListItem->save();

            return redirect()->back()->with('success', __('Продукт успешно добавлен в лист сравнения'));
        }

        return redirect('login');
    }

    public function remove($productId)
    {
        if (Auth::check()) {
            CompareList::find($productId)->delete();

            return redirect()->back()->with('success', __('Продукт успешно удалён из листа сравнения'));
        }

        return redirect('login');
    }
}
