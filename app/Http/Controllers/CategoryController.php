<?php

namespace App\Http\Controllers;

use App\category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
           'categoryName' => 'required',
           'categoryCode' => 'required'
        ]);

        if ($request->input('categoryId')) {
            $id = $request->input('categoryId');
            $category = category::find($id);
            $this->_setCategory($category, $request);
            return redirect()->back()->with('success', __('Категория успешно изменена'));
        }

        $category = new category();
        $this->_setCategory($category, $request);
        return redirect()->back()->with('success', __('Категория успешно добавлена'));
    }

    public function deleteCategory($id)
    {
        category::find($id)->delete($id);
        return redirect()->back()->with('success', __('Категория удалена'));
    }

    private function _setCategory($category, $request)
    {
        $category->category_name = $request->input('categoryName');
        $category->category_code = $request->input('categoryCode');
        $category->save();
    }
}
