<?php

namespace App\Http\Controllers;

use App\shipping;
use Illuminate\Http\Request;

class ShippingController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
           'сountry' => 'required',
           'posta' => 'required',
           'price' => 'required',
           'description' => 'required',
           'selectstatus' => 'required'
        ]);

        if ($request->input('shippingId')) {
            $id = $request->input('shippingId');
            $shipping = shipping::find($id);
            $this->_setShipping($shipping, $request);
            return redirect('/shop/admin/infoshippingmethods')->with('success', __('Метод доставки успешно изменён'));
        }

        $shipping = new shipping();
        $this->_setShipping($shipping, $request);
        return redirect('/shop/admin/infoshippingmethods')->with('success', __('Метод доставки успешно добавлен'));
    }

    public function deleteShipping($id)
    {
        shipping::find($id)->delete($id);
        return redirect()->back()->with('success', __('Метод доставки успешно удалён'));
    }

    private function _setShipping($shipping, $request)
    {
        switch ($request->input('selectstatus')) {
            case 'active':
                $shipping->is_active = 1;
                break;

            case 'inactive':
                $shipping->is_active = 0;
                break;
        }

        $shipping->shippingCountry = $request->input('сountry');
        $shipping->shippingTitle = $request->input('posta');
        $shipping->shippingDescription = $request->input('description');
        $shipping->shippingPrice = $request->input('price');
        $shipping->save();
    }
}
