<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class GiftCardController extends Controller
{
    public function storeGiftCard(Request $request)
    {
    	$this->validate($request, [
    		'giftcardcode' => 'required',
            'nominal' => 'required',
    		'selectstatus' => 'required'
    	]);

    	$giftcard = new App\giftCard;
    	switch ($request->input('selectstatus')) {
    		case 'active':
    			$giftcard->active = 1;
    			break;

    		case 'inactive':
    			$giftcard->active = 0;
    			break;
    	}
    	$giftcard->giftCardCode = $request->input('giftcardcode');
        $giftcard->nominal = $request->input('nominal');
    	$giftcard->save();
    	return redirect('/shop/admin/infogiftcard')->with('success', __('Код был успешно добавлен'));
    }

    public function verifyGiftCard(Request $request)
    {
    	$this->validate($request, [
    		'verifygiftcard' => array(
            'required',
            'regex:/^([A-Z0-9]+)/'
            ),
    	]);

    	$verify = App\giftCard::where('giftCardCode', $request->input('verifygiftcard'))->get();
        if (isset($verify[0]['giftCardCode'])) {
            if ($verify[0]['giftCardCode'] === $request->input('verifygiftcard')) {
                session()->put('giftcardCode', $verify[0]['nominal']);
                return redirect('/shop/cart')->with('success', __('Успешно добавлено'));
            }
        }

        return redirect('/shop/cart')->with('error', __('Возникла ошибка при добавлении'));
    }

    public function removeGiftCard()
    {
        if (session()->exists('giftcardCode')) {
            session()->forget('giftcardCode');
            return redirect('/shop/cart')->with('back', __('Отмена использования карты'));
        }
    }

    public function deleteGiftCard($id)
    {
    	App\giftCard::find($id)->delete();
    	return redirect()->back()->with('success', __('Подарочная карта успешно удалена'));
    }
}
