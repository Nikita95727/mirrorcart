<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class ShopController extends Controller
{
    public function index()
    {
        $products = App\product::paginate(9);
        $categories = App\category::all();
        return view('shop.index', compact(array(
            'products', 'categories'
        )));
    }

    public function buy($id)
    {
        $product = App\product::find($id);
        $cart = session()->get('cart');

        if (!$cart) {
            $cart = array(
              $id => array(
                  'id' => $product->getId(),
                  'image' => $product->getImage(),
                  'title' => $product->getTitle(),
                  'category_code' => $product->category_code,
                  'qty' => 1,
                  'price' => $product->getPrice()
              )
            );

            session()->put('cart', $cart);
        }

        if (isset($cart[$id])) {
            $cart[$id]['qty'] = $cart[$id]['qty']+1;
            session()->put('cart', $cart);
        }

        $cart[$id] = array(
            'id' => $product->getId(),
            'image' => $product->getImage(),
            'title' => $product->getTitle(),
            'category_code' => $product->category_code,
            'qty' => 1,
            'price' => $product->getPrice()
        );

        session()->put('cart', $cart);

        return redirect('/shop')->with('success', __('Продукт успешно добавлен в корзину'));
    }

    public function remove($id)
    {
        $cart = session()->get('cart');
        unset($cart[$id]);
        session()->put('cart', $cart);
        return redirect('/shop/cart')->with('success', __('Продукт успешно убран из корзины'));
    }

    public function cart()
    {
        $cart = new App\cart;
        $product = session()->get('product');
        $shippingMethods = App\shipping::all();
        return view('shop.cart',compact(array(
            'product', 'cart', 'shippingMethods'
        )));
    }

    public function contacts()
    {
        return view('staticPages.contacts');
    }
}
