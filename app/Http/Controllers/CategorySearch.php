<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class CategorySearch extends Controller
{
    public function categorySearch($category)
    {
        $products = App\product::where('categories_id', $category)->get();
        $categories = $this->_getCategoryModel();
        return view('shop.catalogsearch', compact(array(
            'products', 'categories'
        )));
    }

    public function sortDesc()
    {
        $products = $this->_getProductModel()->sortByDesc('price');
        $categories = $this->_getCategoryModel();
        return view('shop.index', compact(array(
            'products', 'categories'
        )));
    }

    public function sortAsc()
    {
        $products = $this->_getProductModel()->sortBy('price');
        $categories = $this->_getCategoryModel();
        return view('shop.index', compact(array(
            'products', 'categories'
        )));
    }

    private function _getProductModel()
    {
        return App\product::all();
    }

    private function _getCategoryModel()
    {
        return App\category::all();
    }
}
