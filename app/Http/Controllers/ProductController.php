<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Excel;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'productCategory' => 'required',
            'productVendor' => 'required',
            'description' => 'required',
            'image' => 'required',
            'price' => 'required'
        ]);

        if ($request->input('productId')) {
            $id = $request->input('productId');
            $product = App\product::find($id);
            $this->_setProduct($product, $request);
            return redirect('/shop/admin/showItems')->with('success', __('Продукт был успешно изменён'));
        }

        $product = new App\product;
        $this->_setProduct($product, $request);
        return redirect('/shop/admin/showItems')->with('success', __('Продукт был успешно создан'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = App\product::find($id);
        $products = $this->getRandomProducts();
        $shippingMethods = App\shippment::all();
        return view('shop.show',compact(array('product', 'products', 'shippingMethods')));
    }

    public function show_wishes($id)
    {
        $product = App\product::find($id);
        $products = $this->getRandomProducts();
        $shippingMethods = App\shippment::all();
        return view('dashboard.wishes',compact(array('product', 'products', 'shippingMethods')));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        App\product::find($id)->delete();
        return redirect()->back()->with('success', __('Продукт был успешно удалён'));
    }

    public function getRandomProducts()
    {
        return App\product::all()
            ->shuffle()
            ->slice(0,4);
    }

    public function import(Request $request)
    {
        $this->validate($request, [
           'importFile' => 'required'
        ]);

        Excel::import(new App\Imports\ProductImport(), $request->file('importFile'));

        return redirect('/shop/admin/showItems');
    }

    public function export()
    {
        return Excel::download(new App\Exports\ProductExport(), 'products.xlsx');
    }

    public function addcomment(Request $request)
    {
        $comment = new App\comments();
        $comment->user_id = $request->post('userId');
        $comment->product_id = $request->post('productId');
        $comment->text = $request->post('inputComment');
        $comment->save();
        return redirect()->back()->with('success', __('Коментарий успешно добавлен'));
    }

    private function _setProduct($product, $request)
    {
        $product->title = $request->input('title');
        $product->categories_id = $request->input('productCategory');
        $product->vendor_id = $request->input('productVendor');
        $product->description = $request->input('description');
        $product->image = $request->input('image');
        $product->price = $request->input('price');
        $product->save();
        return redirect()->back()->with('success', __('Продукт успешно добавлен в список желаний'));
    }
}
