<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CartController extends Controller
{
    public function addQty($id)
    {
        $cart = $this->_getSession();
        $cart[$id]['qty'] = $cart[$id]['qty']+1;
        session()->put('cart', $cart);
        return redirect()->back();
    }

    public function minusQty($id)
    {
        $cart = $this->_getSession();
        $cart[$id]['qty'] = $cart[$id]['qty']-1;

        if ($cart[$id]['qty'] == 0) {
            unset($cart[$id]);
        }

        session()->put('cart', $cart);
        return redirect()->back();
    }

    public function clearAll()
    {
        if (session()->exists('cart')) {
            session()->forget('cart');
            return redirect()->back()->with('success', __('Корзина пуста'));
        }

        return redirect()->back()->with('error', __('Произошла ошибка'));
    }

    private function _getSession()
    {
        return session()->get('cart');
    }
}
