<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\comments;

class product extends Model
{
    protected $fillable = ['image', 'title', 'description', 'price', 'categories_id', 'vendor_id'];

    public function attributes()
    {
        return $this->hasMany('App\attributes', 'id');
    }

    public function category()
    {
        return $this->hasMany('App\category', 'id', 'categories_id');
    }

    public function vendor()
    {
        return $this->hasOne('App\vendor', 'id', 'vendor_id');
    }

    public function image()
    {
        return $this->hasMany('App\images', 'products_id', 'id');
    }

    public function comments()
    {
        return $this->hasMany('App\comments', 'product_id');
    }

    public function getComments()
    {
        return $this->comments;
    }

    public function setCategory($category)
    {
    	$this->category = $category;
    }

    public function getCategory()
    {
    	return $this->category[0]->category_name;
    }

    public function setVendor($vendor)
    {
    	$this->vendor = $vendor;
    }

    public function getVendor()
    {
    	return $this->vendor->vendor_name;
    }

    public function setImage($image)
    {
    	$this->image = $image;
    }

    public function setTitle($title)
    {
    	$this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getProductsByCategory($category)
    {
        return $this::where('category_code', $category)->get();
    }
}
