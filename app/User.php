<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname', 'email', 'customer_birthdate', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    private const ADMIN_GROUP_ID = 1;
    private const CONTENT_MANAGER_GROUP_ID = 2;

    public function wishlist()
    {
        return $this->hasOne('App\wishlist');
    }

    public function shippingAddress()
    {
        return $this->hasMany('App\shippingAddress', 'user_id', 'id');
    }

    public function role()
    {
        return $this->hasOne('App\role','id','role_id');
    }

    public function comparelist()
    {
        return $this->hasMany('App\CompareList', 'user_id', 'id');
    }

    public function comments()
    {
        $this->hasMany('App\comments', 'user_id', 'id');
    }

    public function getCompareList()
    {
        return $this->comparelist;
    }

    public function getRoleName()
    {
        return $this->role->roleName;
    }

    public function hasAccess()
    {
        if (($this->role->id === self::ADMIN_GROUP_ID) || ($this->role->id === self::CONTENT_MANAGER_GROUP_ID)) {
            return true;
        }

        return false;
    }

    public function isContentManager()
    {
        if ($this->role->id === self::CONTENT_MANAGER_GROUP_ID) {
            return true;
        }

        return false;
    }

    public function isAdmin()
    {
        if ($this->role->id === self::ADMIN_GROUP_ID) {
            return true;
        }

        return false;
    }


    public function getUserRoles()
    {
        return role::all();
    }

    public function getUserAddresses()
    {
        return $this->shippingAddress;
    }

    public function getComments()
    {
        return $this->comments;
    }
}
