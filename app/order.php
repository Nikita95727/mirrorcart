<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    public function setProduct($product)
    {
    	$this->product = $product;
    }

    public function getProduct()
    {
    	return $this->product;
    }

    public function setCustomer($customer)
    {
    	$this->customer = $customer;
    }

    public function getCustomer()
    {
    	return $this->customer;
    }

    public function setShipping($shipping)
    {
    	$this->shipping = $shipping;
    }

    public function getShipping()
    {
    	return $this->shipping;
    }

    public function setFinalPrice($finalprice)
    {
    	$this->finalprice = $finalprice;
    }

    public function getFinalPrice()
    {
    	return $this->finalprice;
    }


}
