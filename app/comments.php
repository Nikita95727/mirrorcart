<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class comments extends Model
{
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function product()
    {
        return $this->hasOne('App\product');
    }

    public function getUserName()
    {
        return $this->user->name;
    }
}
