<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class shippment extends Model
{
    public function setShippmentName($shippmentName)
    {
    	$this->shippmentName = $shippmentName;
    }

    public function getShippmentName()
    {
    	return $this->shippmentName;
    }

    public function setShippmentDescription($shippmentDescription)
    {
    	$this->shippmentDescription = $shippmentDescription;
    }

    public function getShippmentDescription()
    {
    	return $this->shippmentDescription;
    }

    public function setShippmentPrice($shippmentPrice)
    {
    	$this->shippmentPrice = $shippmentPrice;
    }

    public function getShippmentPrice()
    {
    	return $this->shippmentPrice;
    }
}
