<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompareList extends Model
{
    public function user()
    {
        return $this->hasOne('App\User', 'user_id', 'id');
    }

    public function product()
    {
        return $this->hasOne('App\product', 'id', 'product_id');
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getProduct()
    {
        return $this->product;
    }
}
