$( function() {
    $( "#slider-range" ).slider({
      range: true,
      min: 10,
      max: 50000,
      values: [ 7050, 30000 ],
      slide: function( event, ui ) {
        $( "#amount" ).val( ui.values[ 0 ] +" грн - " +  ui.values[ 1 ] + " грн ");
      }
    });
     $( "#amount" ).val($( "#slider-range" ).slider( "values", 0 ) + " грн - " +  $( "#slider-range" ).slider( "values", 1 ) + " грн ");
  } );
