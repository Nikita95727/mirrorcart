const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css')
    .sass('resources/sass/mainPage.scss', 'public/css')
    .sass('resources/sass/catalog/catalog-page.scss', 'public/css/catalog')
    .sass('resources/sass/catalog/pdp-page.scss', 'public/css/catalog')
    .sass('resources/sass/checkout/cart.scss', 'public/css/checkout')
    .sass('resources/sass/customer/customer-auth.scss', 'public/css/customer')
    .sass('resources/sass/customer/customer-reg.scss', 'public/css/customer')
    .sass('resources/sass/dashboard/dashboard.scss', 'public/css/dashboard');
