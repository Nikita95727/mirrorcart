@extends('layouts.main')
@section('content')
    <div class="row account-dashboard">
        @include('layouts.dashboard.menu')
        <div class="col-md-8">
            @if (session()->exists('success'))
                <div class="alert alert-success" role="alert">
                    {{session()->get('success')}}
                </div>
            @endif
            @if (session()->exists('error'))
                <div class="alert alert-error" role="alert">
                    {{session()->get('success')}}
                </div>
            @endif
            <h3>{{__('Мои данные')}}</h3>
            <div class="row">
                <div class="col-3">
                    @if (Auth::user()->avatar)
                        <img src="{{asset(Auth::user()->avatar)}}" width="200px" height="200px"/>
                    @else
                        <img src="{{asset('img/default-user-avatar.jpg')}}" width="200px" height="200px">
                    @endif
                    <a href="#" class="btn btn-primary change-photo-btn" data-toggle="modal" data-target="#editPhotoModal">{{__('Изменить фото профиля')}}</a>
                    <div class="modal" tabindex="-1" id="editPhotoModal" aria-labelledby="exampleModalLabel" aria-hidden="true" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">{{__('Изменение фото профиля')}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form method="post" action="{{URL::to('/dashboard/changeAvatar')}}">
                                    <div class="modal-body">
                                        @csrf
                                        <input type="hidden" name="userId" value="{{Auth::user()->id}}">
                                        <input type="text" name="userAvatar" class="form-control">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary">{{__('Обновить фото')}}</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Отмена')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-xl-8 col-lg-8">
                    <div class="dashboard-user-fullname">
                        <span>{{__('Полное имя: '.Auth::user()->name.' '.Auth::user()->surname)}}
                            <a href="#" data-toggle="modal" data-target="#editNameModal"><i class="fas fa-pencil-alt"></i></a>
                            <div class="modal" tabindex="-1" id="editNameModal" aria-labelledby="exampleModalLabel" aria-hidden="true" role="dialog">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">{{__('Изменение имени')}}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form method="post" action="{{URL::to('/dashboard/changeName')}}">
                                            <div class="modal-body">
                                                @csrf
                                                <input type="hidden" name="userId" value="{{Auth::user()->id}}">
                                                <label for="userName">{{__('Имя')}}</label>
                                                <input type="text" name="userName" class="form-control" value="{{Auth::user()->name}}">
                                                <label for="userSurname">{{__('Фамилия')}}</label>
                                                <input type="text" name="userSurname" class="form-control" value="{{Auth::user()->surname}}">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary">{{__('Обновить имя')}}</button>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Отмена')}}</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </span>
                    </div>
                    <div class="dashboard-user-birthdate">
                        <span>{{__('Дата рождения: '.Auth::user()->customer_birthdate)}}
                            <i class="fas fa-pencil-alt"></i>
                        </span>
                    </div>
                    <div class="dashboard-user-email">
                        <span>{{__('Электронный адрес: '.Auth::user()->email)}}
                            <a href="#" data-toggle="modal" data-target="#editEmail">
                                <i class="fas fa-pencil-alt"></i>
                            </a>
                            <div class="modal" tabindex="-1" id="editEmail" aria-labelledby="exampleModalLabel" aria-hidden="true" role="dialog">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">{{__('Изменение адреса электронной почты')}}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form method="post" action="{{URL::to('/dashboard/changeEmail')}}">
                                            <div class="modal-body">
                                                @csrf
                                                <input type="hidden" name="userId" value="{{Auth::user()->id}}">
                                                <label for="userEmail">{{__('Адрес электронной почты')}}</label>
                                                <input type="text" name="userEmail" class="form-control" value="{{Auth::user()->email}}">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary">{{__('Обновить адрес электронной почты')}}</button>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Отмена')}}</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
