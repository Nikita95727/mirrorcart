<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('css/layout.css')}}">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </head>
    <body>
        @if (Route::has('login'))
            <div class="top-right links">
                @auth
                    <a href="{{ url('/home') }}">Home</a>
                @else
                    <a href="{{ route('login') }}">Login</a>

                    @if (Route::has('register'))
                        <a href="{{ route('register') }}">Register</a>
                    @endif
                @endauth
            </div>
        @endif
        <header class="store-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="store-logo">
                            <img class="store-logo-img" src="https://vishivkabiserom.com.ua/image/cache/data/dl2//cb1AK3-256%20%D0%9B%D0%BE%D0%B2%D0%B5%D1%86%20%D1%81%D0%BD%D0%BE%D0%B2.%20%D0%9C%D0%B5%D1%87%D1%82%D1%8B-1000x1000.png">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="store-name">
                            <h1>Мастерская Крымгильда</h1>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="store-phone">
                            <a href="tel:+38(066)306-50-02">+38(066)306-50-02</a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <nav class="navbar navbar-expand-lg navbar-light bg-light">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNavDropdown">
                            <ul class="navbar-nav">
                                <li class="nav-item active">
                                    <a class="nav-link" href="/">Главная <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/shop">Ассортимент</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">О нас</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Помощь</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/shop/contacts">Контакты</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
                <div class="col-md-3">
                    <div class="mini-cart">
                        <h3><a href="/shop/bucket">Корзина</a></h3>
                        <?php if (session()->exists('item')){ ?>
                        <?php $item = session()->get('item') ?>
                        <span>Товаров: <?php echo count($item) ?></span><br>
                        <span>Сумма товаров: <?php echo $item->itemPrice ?> грн</span>
                        <?php }?>
                        <?php if (!session()->exists('item')) { ?>
                        <span>Корзина пуста</span>
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="accordion" id="accordionExample">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h2 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <h2>Каталог</h2>
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body">
                                    <nav class="nav flex-column">
                                        <a class="nav-link active" href="#">Ловцы</a>
                                        <a class="nav-link" href="#">Брелки</a>
                                        <a class="nav-link" href="#">Браслеты</a>
                                        <a class="nav-link" href="#">Серьги</a>
                                        <a class="nav-link" href="#">Украшение на шею</a>
                                        <a class="nav-link" href="#">Авторские работы</a>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="alert alert-light" role="alert">
                        <section>
                            <p>
                                Телефон для заказов: +38(066)306-50-02
                            </p>
                            <p>
                                С найлучшими пожеланиями, команда Кримгильды!
                            </p>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
