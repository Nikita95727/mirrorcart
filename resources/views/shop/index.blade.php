@extends('layouts.main')
@section('content')
<div class="container box1 catalog-page">
    <div class="row">
        <div class="col-sm-2">
            @include('layouts.categories.categories')
            @include('layouts.filters.filters')
        </div>
        <div class="col-sm-10 catalog">
            @if (session()->exists('success'))
                <div class="alert alert-success" role="alert">
                    {{session()->get('success')}}
                </div>
            @endif
            <div class="sort-fields">
                <a href="{{'/shop/sort/asc'}}">{{__('Сначала дешевые')}}</a>
                <a href="{{'/shop/sort/desc'}}">{{__('Сначала дорогие')}}</a>
            </div>
            <div class="row">
                @if (count($products) > 0)
                    @foreach ($products as $product)
                        <div class="col-sm-4 block-cards">
                            <div class="card">
                                <a href = "{{URL::to('shop/show/'.$product->getId())}}">
                                    <img class="card-img-top" src="{{$product->getImage()}}" alt="Card image cap">
                                </a>
                                <div class="card-body">
                                    <h5 class="card-title"><a href="{{URL::to('shop/show/'.$product->getId())}}" class="card-link">{{$product->title}}</a></h5><br>
                                    <h4 class="card-subtitle">{{$product->price}} грн </h4><br>
                                    <a href="{{URL::to('/shop/buy/'.$product->getId())}}" class="btn btn-success btn-buy"> Купить </a>
                                    <a href="{{URL::to('/shop/addtowishlist/'.$product->getId())}}" class="wishlist-icon link-icon" title="Добавить в Избранное"><i class="fas fa-heart"></i></a>
                                    <a href="{{URL::to('/shop/comparelist/add/'.$product->getId())}}" class="compare-icon link-icon" title="Добавить к сравнению"><i class="fas fa-balance-scale"></i></a><br>
                                    <h8 class="card-text"> {{$product->description}} </h8>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
