@extends('layouts.main')
@section('content')
    <div class="container cart-container">
        <h1>Корзина</h1>
        @if (session()->exists('success'))
            <div class="alert alert-success" role="alert">
                {{session()->get('success')}}
            </div>
        @endif
        @if (session()->exists('cart'))
        <table class="table table-striped">
            <thead class="thead-primary">
                <tr>
                    <th width="5%" scope="col">{{__('Продукт')}}</th>
                    <th scope="col"></th>
                    <th width="10%" scope="col">{{__('Количество')}}</th>
                    <th width="10%" scope="col">{{__('Стоимость')}}</th>
                    <th width="5%" scope="col"></th>
                </tr>
            </thead>
            <tbody>
            @foreach (session('cart') as $id => $cartItems)
                <tr>
                    <td>
                        <a href="{{URL::to('/shop/show/'.$cartItems['id'])}}">
                            <img src="{{$cartItems['image']}}" height="64px" width="64px">
                        </a>
                    </td>
                    <td>
                        <a href="{{URL::to('/shop/show/'.$cartItems['id'])}}">
                            {{$cartItems['title']}}
                        </a>
                    </td>
                    <td>
                        <a href="{{URL::to('/shop/cart/addqty/'.$cartItems['id'])}}">
                            <i class="fas fa-plus-square"></i>
                        </a>
                        <input type="text" class="qty-input-value" value="{{$cartItems['qty']}}" name="qty-input">
                        <a href="{{URL::to('/shop/cart/minusqty/'.$cartItems['id'])}}">
                            <i class="fas fa-minus-square"></i>
                        </a>
                    </td>
                    <td>{{$cartItems['qty'] * $cartItems['price']}} грн</td>
                    <td><a href="#" data-toggle="modal" data-target="#deleteItem{{$cartItems['id']}}"><i class="fas fa-trash-alt"></i></a></td>
                    <div class="modal fade" id="deleteItem{{$cartItems['id']}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">{{__('Подтверждение удаления')}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <h6>{{__('Вы уверены, что хотите удалить данный продукт из корзины?')}}</h6>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Закрыть')}}</button>
                                    <a href="{{URL::to('/shop/cart/remove/'.$cartItems['id'])}}"><button type="button" class="btn btn-primary">Удалить</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </tr>
            @endforeach
            </tbody>
        </table>
        <a href="{{URL::to('/shop/cart/clearAll')}}" class="btn btn-danger clear-cart-btn">{{__('Очистить корзину')}}</a>
            <div class="container">
                <div class="row">
                    <div class="col-3 special-cards">
                        <div class="form-group">
                            @if(session()->exists('error')): ?>
                            <div class="alert alert-error" role="alert">
                                {{session()->get('error')}}
                            </div>
                            @endif
                                <a href="#" data-toggle="modal" data-target="#addDiscount">
                                    <button class="btn btn-success add-discount-btn">
                                        <i class="fas fa-credit-card"></i> {{__('Дисконт')}}
                                    </button>
                                </a>
                                <div class="modal fade" id="addDiscount" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">{{__('Добавить дисконтную карту')}}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form>
                                                {{csrf_field()}}
                                                <div class="modal-body">
                                                    <input type="text" class="form-control" name="verifydiscountcard" placeholder="{{__('Введите код')}}">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Закрыть')}}</button>
                                                    <button type="submit" class="btn btn-primary">{{__('Добавить')}}</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" data-toggle="modal" data-target="#addGiftCard">
                                    <button class="btn btn-info add-gift-card-btn">
                                        <i class="fas fa-credit-card"></i> {{__('Подарочная карта')}}
                                    </button>
                                </a>
                                <div class="modal fade" id="addGiftCard" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">{{__('Добавить подарочную карту')}}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form method="post" action="{{URL::to('/shop/verifygift')}}">
                                                {{csrf_field()}}
                                                <div class="modal-body">
                                                    <input type="text" class="form-control" name="verifygiftcard" placeholder="{{__('Введите код')}}">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Закрыть')}}</button>
                                                    <button type="submit" class="btn btn-primary">{{__('Добавить')}}</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="back-to-shopping">
                            <a href="{{URL::to('/shop')}}">
                                <button class="back-to-shopping-btn btn btn-dark">{{__('Продолжить покупки')}}</button>
                            </a>
                        </div>
                    </div>
                    <div class="col-5 shipping-methods">
                        <div class="form-group">
                            @if (!empty($shippingMethods))
                                <label for="shipping-method-input">{{__('Выберите метод доставки')}}</label>
                                @foreach ($shippingMethods as $shippingMethod)
                                    <label class="shipping-method-check-label">
                                        <input type="radio" class="form-check-input">{{$shippingMethod->shippingTitle}} - {{$shippingMethod->shippingPrice}} грн
                                    </label>
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="col-4 tax">
                        <div class="check-subtotal">
                            <span class="check-subtotal-header">{{__('Итого:')}}
                                <div class="clear-subtotal-value">
                                    <span class="check-subtotal-span">{{__('Итого без бонусов:')}}
                                        @if (isset($cart))
                                            {{$cart->getTotal()}}
                                        @endif грн
                                    </span>
                                </div>
                                <div class="tax-value">
                                    <span class="check-subtotal-span">{{__('ПДВ:')}}
                                        0 грн
                                    </span>
                                </div>
                                <div class="shipping-cost-value">
                                    <span class="check-subtotal-span">{{__('За доставку:')}}
                                        0 грн
                                    </span>
                                </div>
                                <div class="discount-output-value">
                                    <span class="check-subtotal-span">{{__('Дисконт:')}}
                                        0 грн
                                    </span>
                                </div>
                                @if ($cart->getCupon())
                                    <div class="discount-output-value">
                                        <span class="check-subtotal-span">{{__('Купон:')}}
                                            {{$cart->getCupon()}} грн
                                        </span>
                                    </div>
                                @endif
                                @if ($cart->getGiftCard())
                                    <div class="gift-card-output-value">
                                        <span class="check-subtotal-span">{{__('Подарочная карта:')}}
                                            <span>-{{$cart->getGiftCard()}} грн</span>
                                            <a href="{{URL::to('/shop/removegiftcard')}}" class="remove-gift-card-button">{{__('Отменить')}}</a>
                                        </span>
                                    </div>
                                @endif
                                <div class="total-value">
                                    <span class="check-total-value">{{__('Итоговая стоимость:')}}
                                        {{$cart->getSubtotal()}} грн
                                    </span>
                                </div>
                            </span>
                        </div>
                        <div class="checkout-link">
                            <a href="{{URL::to('/checkout')}}">
                                <button type="button" class="go-to-checkout-btn btn btn-success">{{__('Оформить заказ')}}</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if (!session()->exists('cart'))
            <div class="empty-cart">
                <img src="{{asset('img/empty-cart.png')}}">
                <h5>{{__('Корзина пуста')}}</h5>
                <span>{{__('Похоже, что вы не добавили ещё ни одного товара в корзину.')}}</span>
                <div class="empty-cart-continue-shop-link">
                    <a href="{{URL::to('/shop')}}"><button type="button" class="btn btn-primary">{{__('Продолжить покупки')}}</button></a>
                </div>
            </div>
        @endif
    </div>
@endsection
