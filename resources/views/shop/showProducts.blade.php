@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="row">
            <table class="table table-bordered">
                <thead>
                    <th>Заголовок</th>
                    <th>Описание</th>
                    <th>Изображение</th>
                    <th>Цена</th>
                    <th>Действие</th>
                </thead>
                <?php foreach ($products as $product): ?>
                    <tr>
                        <td>{{$product->title}}</td>
                        <td>{{$product->description}}</td>
                        <td><img src="{{$product->image}}" width="150px" height="150px"></td>
                        <td>{{$product->price}}</td>
                        <td><a href="../../shop/delete/{{$product->id}}">Удалить</a><br><a href="#">Изменить</a></td>
                    </tr>
                <?php endforeach;?>
            </table>
        </div>
        <a href="/shop/admin/createItem"><h6>Добавить новый товар</h6></a>
    </div>
@endsection
