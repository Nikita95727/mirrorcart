@extends('layouts.main')
@section('content')
    <div class="container box1 show-product">
        <div class="row">
            <div class="col-md-4 item-photo">
                <img style="max-width:100%;" src="{{$product->getImage()}}"/>
            </div>
            <div class="col-md-5">

                <h3>{{$product->getTitle()}}</h3>

                <h5 class="title-price">Цена</h5>
                <h3>{{$product->getPrice()}} <span><i class="fas fa-hryvnia"></i></span></h3>

                <div class="section">
                    <h5 class="title-attr">Цвет </h5>
                    <div>
                        <div class="attr"></div>
                        <div class="attr"></div>
                    </div>
                </div>
                <div class="section">
                    <h5 class="title-attr">Кол-во памяти </h5>
                    <div>
                        <div class="attr2">16 GB</div>
                        <div class="attr2">32 GB</div>
                    </div>
                </div>

                <div class="section">
                    <a href="{{URL::to('/shop/buy/'.$product->getId())}}">
                        <button type="button" class="btn btn-primary pdp-add-to-cart-btn">Добавить в корзину</button>
                    </a>
                    <h6><a href="{{URL::to('/shop/addtowishlist/'.$product->getId())}}"><i class="fas fa-hand-holding-heart"></i> Добавить в
                            Избранное</a></h6>
                </div>
                <div class="section add-promo-code">
                    @if (session()->exists('success'))
                        <div class="alert alert-success" role="alert">
                            {{session()->get('success')}}
                        </div>
                    @endif
                    <form method="POST" action="{{URL::to('/shop/verifyCupon')}}">
                        <div class="form-group" >
                            <label for="verifycupon" >У вас есть купон?</label>
                            <input type="text" class="form-control" name="verifycupon" style="width: 100px;"><br>
                            <button type="submit" class="btn btn-primary">{{__('Добавить')}}</button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="col-md-3">
                Способы доставки
                Доставка
                - Новая Почта
                - Ин тайм
                - Деливери
                - Гюнсел
                - Самовывоз в Запорожье
                - Курьером по Запорожью
                Оплата
                - Наличными
                - Безналичный расчет
                - Перевод на банковскую карту
                - Наложенный платеж
                Подробнее о способах оплаты и доставки
                Гарантия
                - Обмен/возврат товара в течение 14 дней
                Подробнее о гарантийном обслуживании
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <ul class="menu-items">
                    <li class="active">Общая информация</li>
                    <li>Все харатеристики</li>
                    <li>Отзывы</li>
                    <li>Доставка</li>
                </ul>
                <div>
                    <p>
                        <small>
                            {{$product->getDescription()}}
                        </small>
                    </p>
                    <small>
                        <ul>
                            <li>Super AMOLED capacitive touchscreen display with 16M colors</li>
                            <li>Available on GSM, AT T, T-Mobile and other carriers</li>
                            <li>Compatible with GSM 850 / 900 / 1800; HSDPA 850 / 1900 / 2100 LTE; 700 MHz Class 17 / 1700 /
                                2100 networks
                            </li>
                            <li>MicroUSB and USB connectivity</li>
                            <li>Interfaces with Wi-Fi 802.11 a/b/g/n/ac, dual band and Bluetooth</li>
                            <li>Wi-Fi hotspot to keep other devices online when a connection is not available</li>
                            <li>SMS, MMS, email, Push Mail, IM and RSS messaging</li>
                            <li>Front-facing camera features autofocus, an LED flash, dual video call capability and a sharp
                                4128 x 3096 pixel picture
                            </li>
                            <li>Features 16 GB memory and 2 GB RAM</li>
                            <li>Upgradeable Jelly Bean v4.2.2 to Jelly Bean v4.3 Android OS</li>
                            <li>17 hours of talk time, 350 hours standby time on one charge</li>
                            <li>Available in white or black</li>
                            <li>Model I337</li>
                            <li>Package includes phone, charger, battery and user manual</li>
                            <li>Phone is 5.38 inches high x 2.75 inches wide x 0.13 inches deep and weighs a mere 4.59 oz
                            </li>
                        </ul>
                    </small>
                </div>
            </div>
            <div class="col-md-4">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-tabs" id="comments" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="comment-tab" data-toggle="tab" href="#comment" role="tab" aria-controls="comment" aria-selected="true">{{__('Комментарии')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="addcomment-tab" data-toggle="tab" href="#addcomment" role="tab" aria-controls="addcomment" aria-selected="false">{{__('Добавить комментарий')}}</a>
                    </li>
                </ul>
                <div class="tab-content" id="CommentsContent">
                    <div class="tab-pane fade show active" id="comment" role="tabpanel" aria-labelledby="comment-tab">
                        @foreach ($product->getComments() as $comment)
                            <div class="row no-gutters comment-field">
                                <div class="media">
                                    <img src="{{asset('img/default-user-avatar.jpg')}}" class="align-self-center rounded-circle" alt="..." width="100px" height="100px">
                                </div>
                                <div class="col-md-10">
                                    <div class="card comment-body">
                                        <div class="card-body">
                                            <h5 class="card-title">{{$comment->getUserName()}}</h5>
                                            <p class="card-text comment-text-{{$comment->id}}">{{$comment->text}}</p>
                                            <p class="card-text"><small class="text-muted">{{ date('Y.m.d', strtotime($comment->created_at))}}</small></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="tab-pane fade" id="addcomment" role="tabpanel" aria-labelledby="addcomment-tab">
                        <div class="row no-gutters comment-field">
                            <form class="form-style" method="post" action="{{URL::to('/shop/product/addcomment')}}">
                                <div class="form-group">
                                    <label for="inputComment">{{__('Ваш комментарий')}}</label>
                                    <textarea class="form-control" name="inputComment" rows="4"></textarea>
                                    <input type="hidden" value="{{ Auth::user()->id }}" name="userId">
                                    <input type="hidden" value="{{ $product->getId() }}" name="productId">
                                </div>
                                {{--<div class="input-group mb-3">--}}
                                {{--<div class="input-group-prepend">--}}
                                {{--<span class="input-group-text" id="basic-addon3">{{__('URL:')}}</span>--}}
                                {{--</div>--}}
                                {{--<input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3">--}}
                                {{--</div>--}}
                                {{--<div class="custom-file">--}}
                                {{--<input type="file" class="custom-file-input" id="customFile">--}}
                                {{--<label class="custom-file-label" for="customFile">Choose file</label>--}}
                                {{--</div>--}}
                                <button type="submit" class="btn btn-primary btn-top">Send</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
