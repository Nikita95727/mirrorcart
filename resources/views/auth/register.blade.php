<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/layout.css')}}">
    <link rel="stylesheet" href="{{asset('css/customer/customer-reg.css')}}">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="{{asset('js/slider.js')}}"></script>
    <!-- <script src="https://www.google.com/recaptcha/api.js" async defer></script> -->
</head>
<body>
    <h2 class="customer-reg-form-header">Create an account</h2>
    <p class="customer-reg-form-additional-information">Please enter the following information to create your account.</p>
    <form action="{{ route('register') }}" method="post" class="customer-reg-form">
        @csrf
        <label for="name">Name<em>*</em></label>
        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
        @if ($errors->has('name'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
        <label for="surname">Surname<em>*</em></label>
        <input id="surname" type="text" class="form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}" name="surname" value="{{ old('surname') }}" required autofocus>
        @if ($errors->has('surname'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('surname') }}</strong>
            </span>
        @endif
        <label for="customer_birthdate">Birthdate<em>*</em></label>
        <input id="customer_birthdate" type="date" class="form-control{{$errors->has('customer_birthdate')  ? ' is-invalid' : '' }}" name="customer_birthdate" value="{{ old('customer_birthdate')}}" required>
        @if ($errors->has('customer_birthdate'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('customer_birthdate') }}</strong>
            </span>
        @endif
        <label for="email">E-mail<em>*</em></label>
        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
        @if ($errors->has('email'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
        <label for="customer_password">Введите пароль</label>
        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
        @if ($errors->has('password'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
        <label for="customer_repeat_password">Повторите пароль</label>
        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
        <div class="g-recaptcha" data-sitekey="6LfMyK0UAAAAANq227L_e8-_cssMb3HcdNzGDt57"></div>
        <button type="submit" class="form-control btn customer-reg-submit">Зарегистрировать</button>
        <!-- <button type="submit" class="form-control customer-reg-submit">Зарегистрировать</button> -->
    </form>

</body>
</html>
