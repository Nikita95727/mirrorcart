<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/layout.css')}}">
    <link rel="stylesheet" href="{{asset('css/admin/admin-dashboard.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Permanent+Marker&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/ae7e200d98.js"></script>
</head>
<body>
    <div class="row">
        @include('layouts.admin.adminNavigation')
        <div class="col-9">
            @include('layouts.admin.topNavigation')
            <div class="action-item">
                <div class="card border-left-primary shadow h-100 py-2">
                    <a href="{{URL::to('/shop/admin/createItem')}}">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Добавить продукт</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="action-item">
                <div class="card border-left-primary shadow h-100 py-2">
                    <a href="{{URL::to('/shop/admin/showItems')}}">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Список продуктов</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="action-item">
                <div class="card border-left-primary shadow h-100 py-2">
                    <a href="{{URL::to('/admin/importproducts')}}">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Импорт продуктов</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="action-item">
                <div class="card border-left-primary shadow h-100 py-2">
                    <a href="{{URL::to('/shop/admin/export')}}">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Экспорт продуктов</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="action-item">
                <div class="card border-left-primary shadow h-100 py-2">
                    <a href="{{URL::to('/shop/admin/managecategories')}}">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Управление категориями</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="action-item">
                <div class="card border-left-primary shadow h-100 py-2">
                    <a href="{{URL::to('/admin/manageVendors')}}">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">{{__('Управление производителями')}}</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
