<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/layout.css')}}">
    <link rel="stylesheet" href="{{asset('css/admin/admin-dashboard.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Permanent+Marker&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/ae7e200d98.js"></script>
</head>
<body>
<div class="row">
    @include('layouts.admin.adminNavigation')
    <div class="col-9">
        @include('layouts.admin.topNavigation')
        <div class="card shadow mb-4">
            @if(session()->exists('success'))
                <div class="alert alert-success" role="alert">
                    {{session()->get('success')}}
                </div>
            @endif
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Имя</th>
                        <th>Фамилия</th>
                        <th>Дата рождения</th>
                        <th>Электронный адрес</th>
                        <th>Адрес</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Имя</th>
                        <th>Фамилия</th>
                        <th>Дата рождения</th>
                        <th>Электронный адрес</th>
                        <th>Адрес</th>
                        <th>Действия</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    <?php foreach ($users as $user): ?>
                    <tr>
                        <td>{{$user->id}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->surname}}</td>
                        <td>{{$user->customer_birthdate}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->customer_shippingAddress}}</td>
                        <td><a href="#"><i class="fas fa-user-minus" title="{{__('Удалить пользователя')}}"></i></a><br><a href="{{URL::to('/shop/admin/editUser/'.$user->id)}}"><i class="fas fa-user-edit" title="{{__('Изменить')}}"></i></a><a href="{{URL::to('/shop/admin/users/showUser/'.$user->id)}}"><i class="fas fa-user-cog"></i></a></td>
                    </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
            </div>
            <a href="{{ URL::to('/shop/admin/createUser') }}"><h6><i class="fas fa-user-plus" title="{{__('Добавить пользователя')}}"></i></h6></a>
            {{$users->links()}}
        </div>
    </div>
</div>
</body>
</html>
