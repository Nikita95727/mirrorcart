<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/layout.css')}}">
    <link rel="stylesheet" href="{{asset('css/admin/admin-dashboard.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Permanent+Marker&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/ae7e200d98.js"></script>
</head>
<body>
<div class="row">
    @include('layouts.admin.adminNavigation')
    <div class="col-9">
        @include('layouts.admin.topNavigation')
        <div class="row">
            @if (session()->exists('success'))
                <div class="alert alert-success" role="alert">
                    {{session()->get('success')}}
                </div>
            @endif
            @if (session()->exists('error'))
                 <div class="alert alert-error" role="alert">
                    {{session()->get('success')}}
                 </div>
            @endif
            <div class="col-4 item-photo">
                @if ($user->avatar)
                    <img src="{{asset($user->avatar)}}" width="200px" height="200px"/>
                    @else
                    <img src="{{asset('img/default-user-avatar.jpg')}}" width="200px" height="200px">
                @endif
            </div>
            <div class="col-md-5">
                <div class="user-name-fio">
                    <span>{{__('Полное имя пользователя: '.$user->name)}}</span> <span>{{$user->surname}}</span>
                </div>
                <div class="user-email">
                    <span>{{__('Электронный адрес: '.$user->email)}}</span>
                </div>
                <div class="user-birthday-date">
                    <span>{{__('Дата рождения: '.$user->customer_birthdate)}}</span>
                </div>
                <div class="user-shipping-addresses">
                    <span>{{__('Адрес(а) пользователя:')}}</span>
                </div>
                @if (Auth::user()->isAdmin())
                <div class="user-role">
                    <span>{{__('Роль пользователя: '.$user->getRoleName())}} <a href="#" data-toggle="modal" data-target="#editRoleModal"><i class="fas fa-user-cog"></i></a></span>
                    <div class="modal" tabindex="-1" id="editRoleModal" aria-labelledby="exampleModalLabel" aria-hidden="true" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">{{__('Изменение роли')}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form method="post" action="{{URL::to('/shop/admin/user/changeRole')}}">
                                    <div class="modal-body">
                                        @csrf
                                        <input type="hidden" name="userId" value="{{$user->id}}">
                                        <select class="form-control" name="userRole">
                                            @foreach($user->getUserRoles() as $role)
                                                <option value="{{$role->id}}">{{$role->roleName}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary">{{__('Назначить')}}</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Отмена')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
        <div class="row">
            <table class="table table-bordered table-info">
                <thead>
                    <th>{{__('Номер заказа')}}</th>
                    <th>{{__('Товары')}}</th>
                    <th>{{__('Общая сумма')}}</th>
                    <th>{{__('Статус')}}</th>
                    <th>{{__('Отследить')}}</th>
                </thead>
            </table>
        </div>
    </div>
</div>
</body>
</html>
