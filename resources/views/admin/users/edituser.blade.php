<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/layout.css')}}">
    <link rel="stylesheet" href="{{asset('css/admin/admin-dashboard.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Permanent+Marker&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/ae7e200d98.js"></script>
</head>
<body>
<div class="row">
    @include('layouts.admin.adminNavigation')
    <div class="col-9">
        @include('layouts.admin.topNavigation')
        <h2 class="customer-reg-form-header">{{__('Редактировать учётную запись')}}</h2>
        <p class="customer-reg-form-additional-information">{{__('Пожалуйста, введите данные для создания учётной записи')}}</p>
        <form action="{{URL::to('/shop/admin/user/store')}}" method="post" class="customer-reg-form">
            @csrf
            <input type="hidden" value="{{$user->id}}" name="userId">
            <label for="name">{{__('Имя')}}<em>*</em></label>
            <input id="name" type="text" class="form-control" name="name" value="{{$user->name}}" required autofocus>
            <label for="surname">{{__('Фамилия')}}<em>*</em></label>
            <input id="surname" type="text" class="form-control" name="surname" value="{{$user->surname}}" required autofocus>
            <label for="customer_birthdate">{{__('Дата рождения')}}<em>*</em></label>
            <input id="customer_birthdate" type="date" class="form-control" name="customer_birthdate" value="{{$user->customer_birthdate}}" required>
            <label for="email">{{__('E-mail')}}<em>*</em></label>
            <input id="email" type="email" class="form-control" name="email" value="{{$user->email}}" required>
            <div class="g-recaptcha" data-sitekey="6LfMyK0UAAAAANq227L_e8-_cssMb3HcdNzGDt57"></div>
            <button type="submit" class="form-control btn customer-reg-submit">{{__('Редактировать')}}</button>
        </form>
    </div>
</div>
</body>
</html>
