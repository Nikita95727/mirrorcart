<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/layout.css')}}">
    <link rel="stylesheet" href="{{asset('css/admin/admin-dashboard.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Permanent+Marker&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/ae7e200d98.js"></script>
</head>
<body>
<div class="row">
    @include('layouts.admin.adminNavigation')
    <div class="col-9">
        @include('layouts.admin.topNavigation')
        <div class="store-config">
            <h4>{{__('Конфигурация магазина')}}</h4>
            @if ($shopconfig)
                @if (session()->exists('success'))
                    <div class="alert alert-success" role="alert">
                        {{session()->get('success')}}
                    </div>
                @endif
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">{{__('Имя магазина')}}</th>
                            <th scope="col">{{__('Телефон магазина')}}</th>
                            <th scope="col">{{__('Логотип магазина')}}</th>
                            <th scope="col">{{__('Электронная почта')}}</th>
                            <th scope="col">{{__('Адрес')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="shop-config-name">
                                {{$shopconfig->shopName}}
                                <a href="#" data-toggle="modal" data-target="#editShopName{{$shopconfig->id}}">
                                    <i class="fas fa-pencil-alt"></i>
                                </a>
                                <div class="modal fade" id="editShopName{{$shopconfig->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">{{__('Изменить имя магазина')}}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form method="post" action="{{URL::to('/shop/storeShopName')}}">
                                                <div class="modal-body">
                                                    <input type="hidden" value="{{$shopconfig->id}}" name="shopconfigId">
                                                    <label for="shopConfigName">{{__('Имя магазина')}}</label>
                                                    <input type="text" class="form-control" value="{{$shopconfig->shopName}}" name="shopConfigName">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Закрыть')}}</button>
                                                    <button type="submit" class="btn btn-primary">{{__('Сохранить имя')}}</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td class="shop-config-phone">
                                {{$shopconfig->shopPhoneNumber}}<br>
                                <a href="#" data-toggle="modal" data-target="#editShopPhoneNumber{{$shopconfig->id}}">
                                    <i class="fas fa-pencil-alt"></i>
                                </a>
                                <div class="modal fade" id="editShopPhoneNumber{{$shopconfig->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">{{__('Изменить номер телефона')}}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form method="post" action="{{URL::to('/shop/storeShopPhoneNumber')}}">
                                                <div class="modal-body">
                                                    <input type="hidden" value="{{$shopconfig->id}}" name="shopconfigId">
                                                    <label for="shopConfigName">{{__('Номер телефона')}}</label>
                                                    <input type="text" class="form-control" value="{{$shopconfig->shopPhoneNumber}}" name="shopConfigPhoneNumber">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Закрыть')}}</button>
                                                    <button type="submit" class="btn btn-primary">{{__('Сохранить телефон')}}</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td class="shop-config-logo">
                                <img src="{{asset($shopconfig->shopLogo)}}" width="64px" height="64px">
                                <a href="#" data-toggle="modal" data-target="#editShopLogo{{$shopconfig->id}}">
                                    <i class="fas fa-pencil-alt"></i>
                                </a>
                                <div class="modal fade" id="editShopLogo{{$shopconfig->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">{{__('Изменить лого')}}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form method="post" action="{{URL::to('/shop/storeShopLogo')}}">
                                                <div class="modal-body">
                                                    <input type="hidden" value="{{$shopconfig->id}}" name="shopconfigId">
                                                    <label for="shopConfigLogo">{{__('Логотип магазина')}}</label>
                                                    <img src="{{asset($shopconfig->shopLogo)}}" width="64px" height="64px">
                                                    <input type="text" class="form-control" name="shopConfigLogo" placeholder="{{__('Введите ссылку на логотип')}}">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Закрыть')}}</button>
                                                    <button type="submit" class="btn btn-primary">{{__('Сохранить лого')}}</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td class="shop-config-address">
                                {{$shopconfig->shopAddress}}<a href="#" data-toggle="modal" data-target="#editShopAddress{{$shopconfig->id}}">
                                    <i class="fas fa-pencil-alt"></i>
                                </a>
                                <div class="modal fade" id="editShopAddress{{$shopconfig->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">{{__('Изменить адрес магазина')}}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form method="post" action="{{URL::to('/shop/storeShopAddress')}}">
                                                <div class="modal-body">
                                                    <input type="hidden" value="{{$shopconfig->id}}" name="shopconfigId">
                                                    <label for="shopConfigName">{{__('Адрес магазина')}}</label>
                                                    <input type="text" class="form-control" value="{{$shopconfig->shopAddress}}" name="shopConfigAddress">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Закрыть')}}</button>
                                                    <button type="submit" class="btn btn-primary">{{__('Сохранить адрес')}}</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td class="shop-email-config">
                                {{$shopconfig->shopEmail}}<a href="#" data-toggle="modal" data-target="#editShopEmail{{$shopconfig->id}}">
                                    <i class="fas fa-pencil-alt"></i>
                                </a>
                                <div class="modal fade" id="editShopEmail{{$shopconfig->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">{{__('Изменить электронную почту магазина')}}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form method="post" action="{{URL::to('/shop/storeShopEmail')}}">
                                                <div class="modal-body">
                                                    <input type="hidden" value="{{$shopconfig->id}}" name="shopconfigId">
                                                    <label for="shopConfigName">{{__('Электронная почта магазина')}}</label>
                                                    <input type="text" class="form-control" value="{{$shopconfig->shopEmail}}" name="shopConfigEmail">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Закрыть')}}</button>
                                                    <button type="submit" class="btn btn-primary">{{__('Сохранить электронную почту')}}</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        @endif
    </div>
</div>
</body>
</html>
