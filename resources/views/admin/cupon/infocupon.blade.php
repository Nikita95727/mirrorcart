<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/layout.css')}}">
    <link rel="stylesheet" href="{{asset('css/admin/admin-dashboard.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Permanent+Marker&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/ae7e200d98.js"></script>
</head>
<body>
<div class="row">
    @include('layouts.admin.adminNavigation')
    <div class="col-9">
        @include('layouts.admin.topNavigation')
        <div class="card shadow mb-4">
            <?php if(session()->exists('success')): ?>
                <div class="alert alert-success" role="alert">
                    {{session()->get('success')}}
                </div>
            <?php endif; ?>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Код купона</th>
                            <th>Номинал</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Код купона</th>
                            <th>Номинал</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                    </tfoot>
                    <tbody>
                       <?php foreach ($cupons as $cupon): ?>
                            <tr>
                                <td>{{$cupon->id}}</td>
                                <td>{{$cupon->cuponCode}}</td>          <!--поля минграции -->
                                <td>{{$cupon->nominal}}</td>
                                <td><?php switch ($cupon->active) {
                                    case '1':
                                        echo 'Активен';
                                        break;
                                    case '0':
                                        echo 'Не активен';
                                        break;
                                } ?></td>
                                <td><a href="{{URL::to('/shop/deletecupon/')}}/{{$cupon->id}}" class="btn btn-danger"><i class="fas fa-trash"></i></a></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <a href="{{URL::to('/shop/admin/addcupon')}}" class="btn btn-success"><h6>Добавить купон</h6></a>
            {{$cupons->links()}}
        </div>
    </div>
</div>
</body>
</html>
