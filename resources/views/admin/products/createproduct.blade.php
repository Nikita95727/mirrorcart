<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/layout.css')}}">
    <link rel="stylesheet" href="{{asset('css/admin/admin-dashboard.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Permanent+Marker&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/ae7e200d98.js"></script>
</head>
<body>
<div class="row">
    @include('layouts.admin.adminNavigation')
    <div class="col-9">
        @include('layouts.admin.topNavigation')
        <form method="post" action="{{URL::to('/item/store')}}" class="product-create-form">
            {{csrf_field()}}
            <div class="form-group">
                <label for="title" title="Обязательное поле">Заголовок*</label>
                <input type="text" class="form-control" name="title" required>
            </div>
            <div class="form-group">
                <label for="category" title="Обязательное поле">Категория*</label>
                <select class="form-control" name="productCategory">
                    @foreach ($categories as $category)
                        <option value="{{$category->id}}">{{$category->category_name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="category" title="Обязательное поле">Производитель*</label>
                <select class="form-control" name="productVendor">
                    @foreach ($vendors as $vendor)
                        <option value="{{$vendor->id}}">{{$vendor->vendor_name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="description" title="Обязательное поле">Описание*</label>
                <textarea class="form-control" name="description"></textarea>
            </div>
            <div class="form-group">
                <label for="image" title="Обязательное поле">Изображение*</label>
                <input type="text" class="form-control" name="image" required>
            </div>
            <div class="form-group">
                <label for="price" title="Обязательное поле">Price*</label>
                <input type="text" class="form-control" name="price" required>
            </div>
            <div class="form-group attributes">
                <label for="attributes">Основные атрибуты</label>
                <input type="text" class="form-control" name="attributes[]" required>
                <div class = 'add-attribute-btn-container'>
                    <a href="#" class="add-attribute-btn">{{__('Добавить атрибут')}}</a>
                </div>
            </div>
            <button type="submit" class="btn btn-success">Добавить товар</button>
        </form>
    </div>
    <script>
        $('.add-attribute-btn').click(function(event) {
            addDynamicExtraField();
            return false;
        });

        function addDynamicExtraField() {
            $('<input type="text" name="attributes[]" class="form-control">').appendTo($('.attributes'));
        }
    </script>
</div>
</body>
</html>
