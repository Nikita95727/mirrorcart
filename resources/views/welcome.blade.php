    @extends('layouts.main')
    @section('welcome')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 slider-main">
            <!--Слайдер-->
                <div class="bd-example">
                    <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                            <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                            <li data-target="#carouselExampleCaptions" data-slide-to="3"></li>
                        </ol>
                        <div class="carousel-inner test">
                            <div class="carousel-item active">
                                <img src="{{asset('img/Test6.jpg')}}" class="d-block w-100" alt="...">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>First slide label</h5>
                                    <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img src="{{asset('img/Test7.jpg')}}" class="d-block w-100" alt="...">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>Second slide label</h5>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img src="{{asset('img/Test8.jpg')}}" class="d-block w-100" alt="...">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>Third slide label</h5>
                                    <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                                </div>
                                      </div>
                            <div class="carousel-item">
                                <img src="{{asset('img/Test9.jpg')}}" class="d-block w-100" alt="...">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>Third slide label</h5>
                                    <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                                </div>
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 box1">
                    <div class="col-sm-12 box1">
                        <div class="card bg-primary">
                            <div class="card-header bg-primary">Меню товаров</div>
                            <div class="list-group list-group-flush">
                                @foreach ($categories as $category)
                                    <a href="{{URL::to('/shop/'.$category->category_code)}}" class="list-group-item list-group-item-action focus-color">{{$category->category_name}}</a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9 box1">
                    <div class="col-md-12 blog box1">
                        <h1>Возможно вас интересует</h1>
                        <div class="card-group">
                            <div class="card">
                                <img src="{{asset('img/Test5.jpg')}}" class="card-img-top img-thumbnail" alt="Top Img">
                                <div class="card-body">
                                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <a href="#" class="btn btn-primary">Просмотр</a>
                                </div>
                            </div>
                            <div class="card">
                                <img src="{{asset('img/Test4.jpg')}}" class="card-img-top img-thumbnail" alt="Top Img">
                                <div class="card-body">
                                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <a href="#" class="btn btn-primary">Просмотр</a>
                                </div>
                            </div>
                            <div class="card">
                                <img src="{{asset('img/Test3.jpg')}}" class="card-img-top img-thumbnail" alt="Top Img">
                                <div class="card-body">
                                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <a href="#" class="btn btn-primary">Просмотр</a>
                                </div>
                            </div>
                            <div class="card">
                                <img src="{{asset('img/Test2.jpg')}}" class="card-img-top img-thumbnail" alt="Top Img">
                                <div class="card-body">
                                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <a href="#" class="btn btn-primary">Просмотр</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-group">
                            <div class="card">
                                <img src="{{asset('img/Test5.jpg')}}" class="card-img-top img-thumbnail" alt="Top Img">
                                <div class="card-body">
                                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <a href="#" class="btn btn-primary">Просмотр</a>
                                </div>
                            </div>
                            <div class="card">
                                <img src="{{asset('img/Test4.jpg')}}" class="card-img-top img-thumbnail" alt="Top Img">
                                <div class="card-body">
                                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <a href="#" class="btn btn-primary">Просмотр</a>
                                </div>
                            </div>
                            <div class="card">
                                <img src="{{asset('img/Test3.jpg')}}" class="card-img-top img-thumbnail" alt="Top Img">
                                <div class="card-body">
                                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <a href="#" class="btn btn-primary">Просмотр</a>
                                </div>
                            </div>
                            <div class="card">
                                <img src="{{asset('img/Test2.jpg')}}" class="card-img-top img-thumbnail" alt="Top Img">
                                <div class="card-body">
                                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <a href="#" class="btn btn-primary">Просмотр</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-group">
                            <div class="card">
                                <img src="{{asset('img/Test5.jpg')}}" class="card-img-top img-thumbnail" alt="Top Img">
                                <div class="card-body">
                                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <a href="#" class="btn btn-primary">Просмотр</a>
                                </div>
                            </div>
                            <div class="card">
                                <img src="{{asset('img/Test4.jpg')}}" class="card-img-top img-thumbnail" alt="Top Img">
                                <div class="card-body">
                                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <a href="#" class="btn btn-primary">Просмотр</a>
                                </div>
                            </div>
                            <div class="card">
                                <img src="{{asset('img/Test3.jpg')}}" class="card-img-top img-thumbnail" alt="Top Img">
                                <div class="card-body">
                                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <a href="#" class="btn btn-primary">Просмотр</a>
                                </div>
                            </div>
                            <div class="card">
                                <img src="{{asset('img/Test2.jpg')}}" class="card-img-top img-thumbnail" alt="Top Img">
                                <div class="card-body">
                                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <a href="#" class="btn btn-primary">Просмотр</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <h1>Shop 1 slider</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <h1>Shop 2 slider</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- todo: make landing page -->
        </div>
        @endsection
