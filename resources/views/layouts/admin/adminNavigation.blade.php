<div class="col-3 admin-dashboard">
    <ul class="navbar-nav bg-gradient-primary">
        <div class="sidebar-brand-text mx-3"><i class="fas fa-angry"></i>{{__('Панель администратора')}}</div>
        <hr class="sidebar-divider my-0">
        @if (Auth::user()->hasAccess())
            @if (Auth::user()->isAdmin())
                <li class="nav-item active">
                    <a class="nav-link" href="{{URL::to('/admin')}}">
                        <span><i class="fas fa-air-freshener"></i>{{__('Продукты')}}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{URL::to('/shop/admin/managecustomers')}}">
                        <span><i class="fas fa-grin-beam-sweat"></i><i class="fas fa-frown-open"></i>{{__('Пользователи')}}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{URL::to('/admin/manageOrders')}}">
                        <span><i class="fas fa-check"></i>{{__('Заказы')}}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{URL::to('/shop/admin/infoshippingmethods')}}">
                        <span><i class="fas fa-shipping-fast"></i>{{__('Способы доставки')}}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{URL::to('/shop/admin/sales')}}">
                        <span><i class="fas fa-piggy-bank"></i>{{__('Продажи')}}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{URL::to('/shop/admin/storeconfig')}}">
                        <span><i class="fas fa-cogs"></i>{{__('Конфигурация магазина')}}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{URL::to('/admin/manageCache')}}">
                        <span><i class="fas fa-cookie"></i>{{__('Управление кэшем')}}</span>
                    </a>
                </li>
            @else
                <li class="nav-item active">
                    <a class="nav-link" href="{{URL::to('/admin')}}">
                        <span><i class="fas fa-air-freshener"></i>{{__('Продукты')}}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{URL::to('/shop/admin/managecustomers')}}">
                        <span><i class="fas fa-grin-beam-sweat"></i><i class="fas fa-frown-open"></i>{{__('Пользователи')}}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{URL::to('/admin/manageOrders')}}">
                        <span><i class="fas fa-check"></i>{{__('Заказы')}}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{URL::to('/shop/admin/infoshippingmethods')}}">
                        <span><i class="fas fa-shipping-fast"></i>{{__('Способы доставки')}}</span>
                    </a>
                </li>
            @endif
        @endif
    </ul>
</div>
