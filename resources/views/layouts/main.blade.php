<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{$storeConfig->shopName}}</title>
    <!-- Fonts -->
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/layout.css')}}">
    <link rel="stylesheet" href="{{asset('css/mainPage.css')}}">
    <link rel="stylesheet" href="{{asset('css/customer/customer-auth.css')}}">
    <link rel="stylesheet" href="{{asset('css/catalog/catalog-page.css')}}">
    <link rel="stylesheet" href="{{asset('css/catalog/pdp-page.css')}}">
    <link rel="stylesheet" href="{{asset('css/dashboard/dashboard.css')}}">
    <link rel="stylesheet" href="{{asset('css/checkout/cart.css')}}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="{{asset('js/slider.js')}}"></script>
    <script src="https://kit.fontawesome.com/ae7e200d98.js"></script>
</head>
<body>
<header>
    <nav class="navbar navbar-expand-sm">
        <ul class="navbar-nav mr-auto">
            <img class="img imag" src="{{asset($storeConfig->shopLogo)}}" alt="logo">
            <h1>{{$storeConfig->shopName}}</h1>
        </ul>
        <ul class="navbar-nav mr-auto">
            <i class="fas fa-phone"> <a href="tel:{{$storeConfig->shopPhoneNumber}}">{{$storeConfig->shopPhoneNumber}}</a></i>
        </ul>
        <ul class="navbar-nav mr-auto">
            <div class="input-group">
                <input class="form-control" type="text" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-primary" type="button"><i class="fas fa-search"></i></button>
                </div>
            </div>
            <li class="nav-item box">
                <a href="{{URL::to('/shop/cart')}}">
                    <button class="btn btn-success ">
                        <i class="fas fa-shopping-cart"></i> Корзина
                        @if (session()->exists('cart'))
                            <span class="badge badge-light cart-items-amount">{{count(session('cart'))}}</span>
                        @endif
                    </button>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <div class="top-right links text-right">
                <!-- Authentication Links -->
                @guest
                    <a class="btn log" role="button" href="{{ route('login') }}">{{ __('Login') }}</a>
                    @if (Route::has('register'))
                        <a class="btn log" role="button" href="{{ route('register') }}">{{ __('Register') }}</a>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="btn log dropdown-toggle buttons" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            <i class="fas fa-user"></i> {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                {{ __('Выйти из аккаунта') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="log">
                                @csrf
                            </form>
                            <a class="dropdown-item" href="{{ URL::to('/dashboard') }}">
                                {{ __('Панель пользователя') }}
                            </a>
                        </div>
                    </li>
                @endguest
            </div>
        </ul>
    </nav>
    <nav class="navbar navbar-expand-sm navbar-custom py-0" >
        <div class="collapse navbar-collapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{URL::to('/')}}"><i class="fas fa-home"></i> Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Каталог</a>
                    <div class="dropdown-menu mega-menu" aria-labelledby="navbarDropdown">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                    @foreach ($categories as $category)
                                        <a class="dropdown-item" id="v-pills-home-tab" href="{{URL::to('/shop/category/'.$category->id)}}" role="tab" aria-controls="v-pills-home" aria-selected="true">{{$category->category_name}} <i class="fas fa-chevron-right category-arrow"></i></a>
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="tab-content" id="v-pills-tabContent">
                                    <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                        <div class="row justify-content-center">
                                            <div class="col-md-4">
                                                <p><strong class="sub-menu-heading"><a href="#">Kids Items 1</a></strong></p>
                                                <p><a href="#">Item Number1</a></p>
                                                <p><a href="#">Item Number1</a></p>
                                                <p><a href="#">Item Number1</a></p>
                                                <p><a href="#">Item Number1</a></p>
                                            </div>
                                            <div class="col-md-4">
                                                <p><strong class="sub-menu-heading"><a href="#">Kids Items 1</a></strong></p>
                                                <p><a href="#">Item Number1</a></p>
                                                <p><a href="#">Item Number1</a></p>
                                                <p><a href="#">Item Number1</a></p>
                                                <p><a href="#">Item Number1</a></p>
                                            </div>
                                            <div class="col-md-4">
                                                <p><strong class="sub-menu-heading"><a href="#">Kids Items 1</a></strong></p>
                                                <p><a href="#">Item Number1</a></p>
                                                <p><a href="#">Item Number1</a></p>
                                                <p><a href="#">Item Number1</a></p>
                                                <p><a href="#">Item Number1</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-content" id="v-pills-tabContent">
                                    <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <p><strong class="sub-menu-heading"><a href="#">Kids Items 1</a></strong></p>
                                                <p><a href="#">Item Number1</a></p>
                                                <p><a href="#">Item Number1</a></p>
                                                <p><a href="#">Item Number1</a></p>
                                                <p><a href="#">Item Number1</a></p>
                                            </div>
                                            <div class="col-md-4">
                                                <p><strong class="sub-menu-heading"><a href="#">Kids Items 1</a></strong></p>
                                                <p><a href="#">Item Number1</a></p>
                                                <p><a href="#">Item Number1</a></p>
                                                <p><a href="#">Item Number1</a></p>
                                                <p><a href="#">Item Number1</a></p>
                                            </div>
                                            <div class="col-md-4">
                                                <p><strong class="sub-menu-heading"><a href="#">Kids Items 1</a></strong></p>
                                                <p><a href="#">Item Number1</a></p>
                                                <p><a href="#">Item Number1</a></p>
                                                <p><a href="#">Item Number1</a></p>
                                                <p><a href="#">Item Number1</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <a href="#">Все категории</a>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <img class="img-top" src="https://image.made-in-china.com/2f0j00NrMUAtTJnQqn/Best-Sale-ATX-Gaming-PC-Case-Computer-Case-with-RGB-Fan-Opened-Glass-Window.jpg" alt="...">
                            </div>
                        </div>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Some menu</a>
                </li>
            </ul>
        </div>
    </nav>
</header>
<div class="content">
    @yield('welcome')
    @yield('content')
</div>
<!-- Footer -->
<footer class="page-footer font-small blue-grey lighten-5">
    <div class="footer footermenu">
        <div class="container">
            <!-- Grid row-->
            <div class="row py-4 d-flex align-items-center">
                <!-- Grid column -->
                <div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">
                    <h5 class="mb-0 white-text-footer">Get connected with us on social networks!</h5>
                </div>
                <!-- Grid column -->
                <div class="col-md-6 col-lg-7 text-center text-md-right">
                    <!-- Facebook -->
                    <a class="fb-ic fs">
                        <i class="fab fa-facebook-f white-text mr-4"> </i>
                    </a>
                    <!-- Twitter -->
                    <a class="tw-ic fs">
                        <i class="fab fa-twitter white-text mr-4"> </i>
                    </a>
                    <!-- Google +-->
                    <a class="gplus-ic fs">
                        <i class="fab fa-google-plus-g white-text mr-4"> </i>
                    </a>
                    <!--Linkedin -->
                    <a class="li-ic fs">
                        <i class="fab fa-linkedin-in white-text mr-4"> </i>
                    </a>
                    <!--Instagram-->
                    <a class="ins-ic fs">
                        <i class="fab fa-instagram white-text"> </i>
                    </a>
                </div>
                <!-- Grid column -->
            </div>
            <!-- Grid row-->
        </div>
    </div>
    <!-- Footer Links -->
    <div class="container text-center text-md-left mt-5">
        <!-- Grid row -->
        <div class="row mt-3 dark-grey-text">
            <!-- Grid column -->
            <div class="col-md-6 col-lg-6 col-xl-6 mb-6">
                <!-- Content -->
                <h6 class="text-uppercase font-weight-bold">Company name</h6>
                <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto he">
                <p>Here you can use rows and columns to organize your footer content. Lorem ipsum dolor sit amet,
                    consectetur
                    adipisicing elit.
                </p>
            </div>
            <!-- Grid column -->
            <div class="col-md-6 col-lg-6 col-xl-6 mx-auto mb-md-0 mb-6">
                <!-- Links -->
                <h6 class="text-uppercase font-weight-bold">Contact</h6>
                <hr class="teal accent-6 mb-6 mt-0 d-inline-block mx-auto he">
                <p><i class="fas fa-home mr-3"></i>{{$storeConfig->shopEmail}}</p>
                <p><i class="fas fa-envelope mr-3"></i>{{$storeConfig->shopAddress}}</p>
                <p><i class="fas fa-phone mr-3"></i>{{$storeConfig->shopPhoneNumber}}</p>
            </div>
            <!-- Grid column -->
        </div>
        <!-- Grid row -->
    </div>
    <!-- Copyright -->
    <div class="footer-copyright text-center text-black-50 py-3">© 2019 Copyright:
        <a class="dark-grey-text" href="#">MirrorWeb</a>
    </div>
    <!-- Copyright -->
</footer>
<!-- Footer -->
</body>
</html>


