<div class="col-md-2 col-lg-2 col-xl-2">
    <div class="dashboard-menu">
        <ul>
            <li><a href="{{URL::to('/dashboard')}}">{{__('Мои заказы')}}</a></li>
            <li><a href="{{URL::to('/dashboard/info')}}">{{__('Информация')}}</a></li>
            <li><a href="{{URL::to('/dashboard/address')}}">{{__('Адресная книга')}}</a></li>
            <li><a href="{{URL::to('/dashboard/equal')}}">{{__('Сравнения')}}</a></li>
            <li><a href="{{URL::to('/dashboard/wishes')}}">{{__('Список желаний')}}</a></li>
        </ul>
    </div>
</div>
