<div class="categories">
    <div class="card bg-primary">
        @if (count($categories) > 0)
            <div class="card-header bg-primary">{{__('Категории')}}</div>
            <div class="list-group list-group-flush">
                @foreach ($categories as $category)
                    <a href="{{URL::to('/shop/category/'.$category->id)}}" class="list-group-item list-group-item-action focus-color">{{$category->category_name}}</a>
                @endforeach
            </div>
        @endif
    </div>
</div>
